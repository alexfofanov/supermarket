# Pipelines:

## dev: build & push -> deploy
## (all other) feature branches: test (on shared runners)

# Stages

stages:
  - build
  - deploy
  - deploy-cleanup

# Variables

variables:
  IMAGE: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}
  IMAGE_SHA: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}
  DEPLOY_DIR: /srv/supermarket
  DEPLOY_LOCK_FILE: .deploy-lock
  SERVICE: backend

# Stage templates

.deploy_template: &deploy_definition
  stage: deploy
  script:
    - cd ${DEPLOY_DIR}
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker-compose pull ${SERVICE}
    - while [ -f ${DEPLOY_LOCK_FILE} ]; do echo "Other deployment is in progress. Waiting..."; sleep 3; done
    - touch ${DEPLOY_LOCK_FILE}
    - docker-compose rm -f -s ${SERVICE}
    - docker-compose run -T --no-deps --rm ${SERVICE} run_migrations
    - docker-compose up -d --no-recreate ${SERVICE}
  after_script:
    - cd ${DEPLOY_DIR}
    - rm ${DEPLOY_LOCK_FILE} || true

.clean_up_template: &clean_up_definition
  script:
    - docker ps --filter status=dead --filter status=exited -aq | xargs -r -I@ docker rm -v @ || true
    - docker images -qf dangling=true | xargs -r -I@ docker rmi @ || true
    - docker volume ls -qf dangling=true | xargs -r -I@ docker volume rm @ || true
  allow_failure: true

### Build image (for dev, staging and master branches) and then push to registry

build:
  stage: build
  image: callstats/docker-with-bash
  variables:
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  script:
    - source ./docker/scripts/ci-env-setup.sh
    - |
      Print_CI_Message "BUILDING RELEASE IMAGE FOR ENVIRONMENT: ${ENVIRONMENT}"
      docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
      Print_CI_Message "PREPARING CACHE IMAGES"
      CACHE_FROM=""
      Print_CI_Message "SUPERVISORD"
      CACHE_FROM_IMAGE="${CI_REGISTRY_IMAGE}/supervisord:cache"
      if ! docker pull ${CACHE_FROM_IMAGE} ; then
          docker build --target supervisor-builder -t ${CACHE_FROM_IMAGE} .
          docker push ${CACHE_FROM_IMAGE}
      fi
      CACHE_FROM="${CACHE_FROM} --cache-from ${CACHE_FROM_IMAGE}"
      CACHE_FROM_IMAGE="${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}"
      if docker pull ${CACHE_FROM_IMAGE} ; then
        CACHE_FROM="${CACHE_FROM} --cache-from ${CACHE_FROM_IMAGE}"
      fi
      Print_CI_Message "APPLICATION"
      docker build ${CACHE_FROM} --target production -t ${IMAGE} -t ${IMAGE_SHA} .
    - |
      Print_CI_Message "PUSHING IMAGE TO REGISTRY"
      docker push ${IMAGE}
      docker push ${IMAGE_SHA}
  only:
    - dev
    - staging
    - training
    - master
  tags:
    - shared
    - docker

### Deploy

deploy:
  <<: *deploy_definition
  environment:
    name: Production
    url: 'https://supermarket.aleksfofanov.me'
  only:
    - master
  tags:
    - supermarket
    - production

### Cleanup

deploy_cleanup:
  <<: *clean_up_definition
  stage: deploy-cleanup
  only:
    - master
  tags:
    - supermarket
    - production
