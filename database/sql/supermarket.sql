/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     24.11.2019 23:21:53                          */
/*==============================================================*/


/*==============================================================*/
/* Domain: Specialization                                       */
/*==============================================================*/
create domain Specialization as TEXT;

/*==============================================================*/
/* Table: Address                                               */
/*==============================================================*/
create table Address (
   address_id           SERIAL               not null,
   street_id            INT4                 not null,
   city_id              INT4                 not null,
   country_id           INT4                 not null,
   region_id            INT4                 not null,
   postal_code          TEXT                 not null,
   building_number      TEXT                 not null,
   literal              TEXT                 null,
   office               TEXT                 null,
   constraint PK_ADDRESS primary key (address_id)
);

/*==============================================================*/
/* Index: Address_PK                                            */
/*==============================================================*/
create unique index Address_PK on Address (
address_id
);

/*==============================================================*/
/* Index: AddressCountry_FK                                     */
/*==============================================================*/
create  index AddressCountry_FK on Address (
country_id
);

/*==============================================================*/
/* Index: AddressRegion_FK                                      */
/*==============================================================*/
create  index AddressRegion_FK on Address (
region_id
);

/*==============================================================*/
/* Index: AddressCity_FK                                        */
/*==============================================================*/
create  index AddressCity_FK on Address (
city_id
);

/*==============================================================*/
/* Index: AddressStreet_FK                                      */
/*==============================================================*/
create  index AddressStreet_FK on Address (
street_id
);

/*==============================================================*/
/* Table: City                                                  */
/*==============================================================*/
create table City (
   city_id              SERIAL               not null,
   name                 TEXT                 not null,
   constraint PK_CITY primary key (city_id)
);

/*==============================================================*/
/* Index: City_PK                                               */
/*==============================================================*/
create unique index City_PK on City (
city_id
);

/*==============================================================*/
/* Table: Country                                               */
/*==============================================================*/
create table Country (
   country_id           SERIAL               not null,
   name                 TEXT                 not null,
   constraint PK_COUNTRY primary key (country_id)
);

/*==============================================================*/
/* Index: Country_PK                                            */
/*==============================================================*/
create unique index Country_PK on Country (
country_id
);

/*==============================================================*/
/* Table: Employee                                              */
/*==============================================================*/
create table Employee (
   employee_id          SERIAL               not null,
   address_id           INT4                 not null,
   specialization       Specialization       not null,
   last_name            TEXT                 not null,
   first_name           TEXT                 not null,
   middle_name          TEXT                 null,
   salary               DECIMAL(12,2)        not null,
   employment_date      DATE                 not null,
   constraint PK_EMPLOYEE primary key (employee_id)
);

/*==============================================================*/
/* Index: Employee_PK                                           */
/*==============================================================*/
create unique index Employee_PK on Employee (
employee_id
);

/*==============================================================*/
/* Index: EmployeeAddress_FK                                    */
/*==============================================================*/
create  index EmployeeAddress_FK on Employee (
address_id
);

/*==============================================================*/
/* Table: Producer                                              */
/*==============================================================*/
create table Producer (
   producer_id          INT4                 not null,
   address_id           INT4                 not null,
   name                 TEXT                 not null,
   email                TEXT                 not null,
   last_name            TEXT                 not null,
   first_name           TEXT                 not null,
   middle_name          TEXT                 null,
   contact_phone        TEXT                 not null,
   constraint PK_PRODUCER primary key (producer_id)
);

/*==============================================================*/
/* Index: Producer_PK                                           */
/*==============================================================*/
create unique index Producer_PK on Producer (
producer_id
);

/*==============================================================*/
/* Index: ProducerAddress_FK                                    */
/*==============================================================*/
create  index ProducerAddress_FK on Producer (
address_id
);

/*==============================================================*/
/* Table: Product                                               */
/*==============================================================*/
create table Product (
   product_id           SERIAL               not null,
   producer_id          INT4                 not null,
   category_id          INT4                 not null,
   product_number       INT4                 not null,
   name                 TEXT                 not null,
   price                DECIMAL(12,2)        not null,
   constraint PK_PRODUCT primary key (product_id)
);

/*==============================================================*/
/* Index: Product_PK                                            */
/*==============================================================*/
create unique index Product_PK on Product (
product_id
);

/*==============================================================*/
/* Index: ProductCategory_FK                                    */
/*==============================================================*/
create  index ProductCategory_FK on Product (
category_id
);

/*==============================================================*/
/* Index: ProductProducer_FK                                    */
/*==============================================================*/
create  index ProductProducer_FK on Product (
producer_id
);

/*==============================================================*/
/* Table: ProductCategory                                       */
/*==============================================================*/
create table ProductCategory (
   category_id          SERIAL               not null,
   name                 TEXT                 not null,
   constraint PK_PRODUCTCATEGORY primary key (category_id)
);

/*==============================================================*/
/* Index: ProductCategory_PK                                    */
/*==============================================================*/
create unique index ProductCategory_PK on ProductCategory (
category_id
);

/*==============================================================*/
/* Table: Region                                                */
/*==============================================================*/
create table Region (
   region_id            SERIAL               not null,
   name                 TEXT                 not null,
   constraint PK_REGION primary key (region_id)
);

/*==============================================================*/
/* Index: Region_PK                                             */
/*==============================================================*/
create unique index Region_PK on Region (
region_id
);

/*==============================================================*/
/* Table: Sale                                                  */
/*==============================================================*/
create table Sale (
   sale_id              SERIAL               not null,
   employee_id          INT4                 not null,
   date                 DATE                 not null,
   constraint PK_SALE primary key (sale_id)
);

/*==============================================================*/
/* Index: Sale_PK                                               */
/*==============================================================*/
create unique index Sale_PK on Sale (
sale_id
);

/*==============================================================*/
/* Index: SaleCashier_FK                                        */
/*==============================================================*/
create  index SaleCashier_FK on Sale (
employee_id
);

/*==============================================================*/
/* Table: SaleProduct                                           */
/*==============================================================*/
create table SaleProduct (
   product_id           INT4                 not null,
   sale_id              INT4                 not null,
   price_per_item       DECIMAL(12,2)        not null,
   quantity             INT2                 not null,
   constraint PK_SALEPRODUCT primary key (product_id, sale_id)
);

/*==============================================================*/
/* Index: SaleProduct_PK                                        */
/*==============================================================*/
create unique index SaleProduct_PK on SaleProduct (
product_id,
sale_id
);

/*==============================================================*/
/* Index: ProductSaleProduct_FK                                 */
/*==============================================================*/
create  index ProductSaleProduct_FK on SaleProduct (
product_id
);

/*==============================================================*/
/* Index: SaleProductSale_FK                                    */
/*==============================================================*/
create  index SaleProductSale_FK on SaleProduct (
sale_id
);

/*==============================================================*/
/* Table: Street                                                */
/*==============================================================*/
create table Street (
   street_id            SERIAL               not null,
   name                 TEXT                 not null,
   constraint PK_STREET primary key (street_id)
);

/*==============================================================*/
/* Index: Street_PK                                             */
/*==============================================================*/
create unique index Street_PK on Street (
street_id
);

/*==============================================================*/
/* Table: SuppliedProduct                                       */
/*==============================================================*/
create table SuppliedProduct (
   supply_order_id      INT4                 not null,
   product_id           INT4                 not null,
   price_per_item       DECIMAL(12,2)        not null,
   quantity             INT2                 not null,
   constraint PK_SUPPLIEDPRODUCT primary key (supply_order_id, product_id)
);

/*==============================================================*/
/* Index: SuppliedProduct_PK                                    */
/*==============================================================*/
create unique index SuppliedProduct_PK on SuppliedProduct (
supply_order_id,
product_id
);

/*==============================================================*/
/* Index: ProductSuppliedProduct_FK                             */
/*==============================================================*/
create  index ProductSuppliedProduct_FK on SuppliedProduct (
product_id
);

/*==============================================================*/
/* Index: SuppliedProductSupply_FK                              */
/*==============================================================*/
create  index SuppliedProductSupply_FK on SuppliedProduct (
supply_order_id
);

/*==============================================================*/
/* Table: Supplier                                              */
/*==============================================================*/
create table Supplier (
   supplier_id          SERIAL               not null,
   address_id           INT4                 not null,
   name                 TEXT                 not null,
   email                TEXT                 not null,
   last_name            TEXT                 not null,
   first_name           TEXT                 not null,
   middle_name          TEXT                 null,
   contact_phone        TEXT                 not null,
   constraint PK_SUPPLIER primary key (supplier_id)
);

/*==============================================================*/
/* Index: Supplier_PK                                           */
/*==============================================================*/
create unique index Supplier_PK on Supplier (
supplier_id
);

/*==============================================================*/
/* Index: SupplierAddress_FK                                    */
/*==============================================================*/
create  index SupplierAddress_FK on Supplier (
address_id
);

/*==============================================================*/
/* Table: Supply                                                */
/*==============================================================*/
create table Supply (
   supply_order_id      INT4                 not null,
   employee_id          INT4                 not null,
   date                 DATE                 null,
   constraint PK_SUPPLY primary key (supply_order_id)
);

/*==============================================================*/
/* Index: Supply_PK                                             */
/*==============================================================*/
create unique index Supply_PK on Supply (
supply_order_id
);

/*==============================================================*/
/* Index: InventoriedEmployee_FK                                */
/*==============================================================*/
create  index InventoriedEmployee_FK on Supply (
employee_id
);

/*==============================================================*/
/* Table: SupplyOrder                                           */
/*==============================================================*/
create table SupplyOrder (
   supply_order_id      SERIAL               not null,
   supplier_id          INT4                 not null,
   employee_id          INT4                 not null,
   created_at           DATE                 not null,
   accepted_at          DATE                 null,
   declined_at          DATE                 null,
   decline_reason       TEXT                 null,
   constraint PK_SUPPLYORDER primary key (supply_order_id)
);

/*==============================================================*/
/* Index: SupplyOrder_PK                                        */
/*==============================================================*/
create unique index SupplyOrder_PK on SupplyOrder (
supply_order_id
);

/*==============================================================*/
/* Index: SupplyOrderBuyer_FK                                   */
/*==============================================================*/
create  index SupplyOrderBuyer_FK on SupplyOrder (
employee_id
);

/*==============================================================*/
/* Index: SupplyOrderSupplier_FK                                */
/*==============================================================*/
create  index SupplyOrderSupplier_FK on SupplyOrder (
supplier_id
);

/*==============================================================*/
/* Table: SupplyOrderProduct                                    */
/*==============================================================*/
create table SupplyOrderProduct (
   product_id           INT4                 not null,
   supply_order_id      INT4                 not null,
   price_per_item       DECIMAL(12,2)        not null,
   quantity             INT2                 not null,
   constraint PK_SUPPLYORDERPRODUCT primary key (product_id, supply_order_id)
);

/*==============================================================*/
/* Index: SupplyOrderProduct_PK                                 */
/*==============================================================*/
create unique index SupplyOrderProduct_PK on SupplyOrderProduct (
product_id,
supply_order_id
);

/*==============================================================*/
/* Index: ProductSupplyOrder_FK                                 */
/*==============================================================*/
create  index ProductSupplyOrder_FK on SupplyOrderProduct (
product_id
);

/*==============================================================*/
/* Index: SupplyOrderProduct_FK                                 */
/*==============================================================*/
create  index SupplyOrderProduct_FK on SupplyOrderProduct (
supply_order_id
);

alter table Address
   add constraint FK_ADDRESS_ADDRESSCI_CITY foreign key (city_id)
      references City (city_id)
      on delete cascade on update cascade;

alter table Address
   add constraint FK_ADDRESS_ADDRESSCO_COUNTRY foreign key (country_id)
      references Country (country_id)
      on delete cascade on update cascade;

alter table Address
   add constraint FK_ADDRESS_ADDRESSRE_REGION foreign key (region_id)
      references Region (region_id)
      on delete cascade on update cascade;

alter table Address
   add constraint FK_ADDRESS_ADDRESSST_STREET foreign key (street_id)
      references Street (street_id)
      on delete cascade on update cascade;

alter table Employee
   add constraint FK_EMPLOYEE_EMPLOYEEA_ADDRESS foreign key (address_id)
      references Address (address_id)
      on delete cascade on update cascade;

alter table Producer
   add constraint FK_PRODUCER_PRODUCERA_ADDRESS foreign key (address_id)
      references Address (address_id)
      on delete cascade on update cascade;

alter table Product
   add constraint FK_PRODUCT_PRODUCTCA_PRODUCTC foreign key (category_id)
      references ProductCategory (category_id)
      on delete cascade on update cascade;

alter table Product
   add constraint FK_PRODUCT_PRODUCTPR_PRODUCER foreign key (producer_id)
      references Producer (producer_id)
      on delete cascade on update cascade;

alter table Sale
   add constraint FK_SALE_SALECASHI_EMPLOYEE foreign key (employee_id)
      references Employee (employee_id)
      on delete cascade on update cascade;

alter table SaleProduct
   add constraint FK_SALEPROD_PRODUCTSA_PRODUCT foreign key (product_id)
      references Product (product_id)
      on delete cascade on update cascade;

alter table SaleProduct
   add constraint FK_SALEPROD_SALEPRODU_SALE foreign key (sale_id)
      references Sale (sale_id)
      on delete cascade on update cascade;

alter table SuppliedProduct
   add constraint FK_SUPPLIED_PRODUCTSU_PRODUCT foreign key (product_id)
      references Product (product_id)
      on delete cascade on update cascade;

alter table SuppliedProduct
   add constraint FK_SUPPLIED_SUPPLIEDP_SUPPLY foreign key (supply_order_id)
      references Supply (supply_order_id)
      on delete cascade on update cascade;

alter table Supplier
   add constraint FK_SUPPLIER_SUPPLIERA_ADDRESS foreign key (address_id)
      references Address (address_id)
      on delete cascade on update cascade;

alter table Supply
   add constraint FK_SUPPLY_INVENTORI_EMPLOYEE foreign key (employee_id)
      references Employee (employee_id)
      on delete cascade on update cascade;

alter table Supply
   add constraint FK_SUPPLY_ORDERSUPP_SUPPLYOR foreign key (supply_order_id)
      references SupplyOrder (supply_order_id)
      on delete cascade on update cascade;

alter table SupplyOrder
   add constraint FK_SUPPLYOR_SUPPLYORD_EMPLOYEE foreign key (employee_id)
      references Employee (employee_id)
      on delete cascade on update cascade;

alter table SupplyOrder
   add constraint FK_SUPPLYOR_SUPPLYORD_SUPPLIER foreign key (supplier_id)
      references Supplier (supplier_id)
      on delete cascade on update cascade;

alter table SupplyOrderProduct
   add constraint FK_SUPPLYOR_PRODUCTSU_PRODUCT foreign key (product_id)
      references Product (product_id)
      on delete cascade on update cascade;

alter table SupplyOrderProduct
   add constraint FK_SUPPLYOR_SUPPLYORD_SUPPLYOR foreign key (supply_order_id)
      references SupplyOrder (supply_order_id)
      on delete cascade on update cascade;

