<?php

use Illuminate\Database\Seeder;

class ProductCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productcategory')->insert([
            'name' => 'Молочные изделия',
        ]);
        DB::table('productcategory')->insert([
            'name' => 'Кондитерские изделия',
        ]);
    }
}
