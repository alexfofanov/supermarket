<?php

use Illuminate\Database\Seeder;

class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = DB::table('city')
                  ->where('name', 'Томск')
                  ->first();

        $street = DB::table('street')
                    ->where('name', 'пр. Мира')
                    ->first();

        $address = DB::table('address')
                     ->where([
                         'city_id'         => $city->city_id,
                         'street_id'       => $street->street_id,
                         'building_number' => 20
                     ])
                     ->first();

        DB::table('supplier')->insert([
            'name'          => 'KDV Group',
            'email'         => 'tomsk@kdv.ru',
            'first_name'    => 'Мария',
            'last_name'     => 'Федорова',
            'middle_name'   => 'Алексеевна',
            'contact_phone' => '+73822780000',
            'address_id'    => $address->address_id
        ]);

        $city = DB::table('city')
                  ->where('name', 'Томск')
                  ->first();

        $street = DB::table('street')
                    ->where('name', 'пр. Ленина')
                    ->first();

        $address = DB::table('address')
                     ->where([
                         'city_id'         => $city->city_id,
                         'street_id'       => $street->street_id,
                         'building_number' => 76
                     ])
                     ->first();

        DB::table('supplier')->insert([
            'name'          => 'X5 Retail Group',
            'email'         => 'tomsk@x5.ru',
            'first_name'    => 'Пётр',
            'last_name'     => 'Петров',
            'middle_name'   => 'Петрович',
            'contact_phone' => '+73822500000',
            'address_id'    => $address->address_id
        ]);
    }
}
