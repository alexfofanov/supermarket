<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(StreetsTableSeeder::class);
        $this->call(AddressTableSeeder::class);
        $this->call(ProducerTableSeeder::class);
        $this->call(SupplierTableSeeder::class);
        $this->call(EmployeeTableSeeder::class);
        $this->call(ProductCategoryTableSeeder::class);
    }
}
