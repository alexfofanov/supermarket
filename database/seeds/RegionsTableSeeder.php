<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('region')->insert([
            'name' => 'Томская область',
        ]);
        DB::table('region')->insert([
            'name' => 'Кемеровская область',
        ]);
        DB::table('region')->insert([
            'name' => 'Новосибирская область',
        ]);
    }
}
