<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = DB::table('city')
                  ->where('name', 'Томск')
                  ->first();

        $street = DB::table('street')
                    ->where('name', 'Говорова')
                    ->first();

        $address = DB::table('address')
                     ->where([
                         'city_id'         => $city->city_id,
                         'street_id'       => $street->street_id,
                         'building_number' => 46,
                         'literal'         => 1,
                         'office'          => 36
                     ])
                     ->first();

        DB::table('employee')->insert([
            'last_name'       => 'Рогов',
            'first_name'      => 'Максим',
            'middle_name'     => 'Петрович',
            'specialization'  => 'warehouseman',
            'salary'          => 30000,
            'employment_date' => Date::now()
                                     ->subYear()
                                     ->toDate(),
            'address_id'      => $address->address_id
        ]);

        $street = DB::table('street')
                    ->where('name', 'пр. Кирова')
                    ->first();

        $address = DB::table('address')
                     ->where([
                         'city_id'         => $city->city_id,
                         'street_id'       => $street->street_id,
                         'building_number' => 48,
                         'office'          => 24
                     ])
                     ->first();

        DB::table('employee')->insert([
            'last_name'       => 'Клочкова',
            'first_name'      => 'Галина',
            'middle_name'     => 'Ивановна',
            'specialization'  => 'cashier',
            'salary'          => 40000,
            'employment_date' => Date::now()
                                     ->subYears(2)
                                     ->subMonth()
                                     ->toDate(),
            'address_id'      => $address->address_id
        ]);

        $street = DB::table('street')
                    ->where('name', 'Дальнеключевская')
                    ->first();

        $address = DB::table('address')
                     ->where([
                         'city_id'         => $city->city_id,
                         'street_id'       => $street->street_id,
                         'building_number' => 14,
                         'literal'         => 'A',
                         'office'          => 19
                     ])
                     ->first();

        DB::table('employee')->insert([
            'last_name'       => 'Иванов',
            'first_name'      => 'Алексей',
            'middle_name'     => 'Владимирович',
            'specialization'  => 'buyer',
            'salary'          => 80000,
            'employment_date' => Date::now()
                                     ->subYears(5)
                                     ->subMonths(6)
                                     ->toDate(),
            'address_id'      => $address->address_id
        ]);
    }
}
