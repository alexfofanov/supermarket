<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('city')->insert([
            'name' => 'Томск',
        ]);
        DB::table('city')->insert([
            'name' => 'Северск',
        ]);
        DB::table('city')->insert([
            'name' => 'Кемерово',
        ]);
        DB::table('city')->insert([
            'name' => 'Новосибирск',
        ]);
    }
}
