<?php

use Illuminate\Database\Seeder;

class StreetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('street')->insert([
            'name' => 'Предзаводская',
        ]);
        DB::table('street')->insert([
            'name' => 'пр. Мира',
        ]);
        DB::table('street')->insert([
            'name' => 'пр. Ленина',
        ]);
        DB::table('street')->insert([
            'name' => 'Говорова',
        ]);
        DB::table('street')->insert([
            'name' => 'пр. Кирова',
        ]);
        DB::table('street')->insert([
            'name' => 'Дальнеключевская',
        ]);
    }
}
