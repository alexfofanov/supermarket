<?php

use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = DB::table('country')
                    ->where('name', 'Россия')
                    ->first();

        $region = DB::table('region')
                    ->where('name', 'Томская область')
                    ->first();

        $city = DB::table('city')
                  ->where('name', 'Северск')
                  ->first();

        $street = DB::table('street')
                    ->where('name', 'Предзаводская')
                    ->first();

        DB::table('address')->insert([
            'country_id'      => $country->country_id,
            'region_id'       => $region->region_id,
            'city_id'         => $city->city_id,
            'street_id'       => $street->street_id,
            'postal_code'     => '636037',
            'building_number' => 14
        ]);

        $city = DB::table('city')
                  ->where('name', 'Томск')
                  ->first();

        $street = DB::table('street')
                    ->where('name', 'пр. Мира')
                    ->first();

        DB::table('address')->insert([
            'country_id'      => $country->country_id,
            'region_id'       => $region->region_id,
            'city_id'         => $city->city_id,
            'street_id'       => $street->street_id,
            'postal_code'     => '634057',
            'building_number' => 20
        ]);

        $street = DB::table('street')
                    ->where('name', 'пр. Ленина')
                    ->first();

        DB::table('address')->insert([
            'country_id'      => $country->country_id,
            'region_id'       => $region->region_id,
            'city_id'         => $city->city_id,
            'street_id'       => $street->street_id,
            'postal_code'     => '634050',
            'building_number' => 76
        ]);

        $street = DB::table('street')
                    ->where('name', 'Говорова')
                    ->first();

        DB::table('address')->insert([
            'country_id'      => $country->country_id,
            'region_id'       => $region->region_id,
            'city_id'         => $city->city_id,
            'street_id'       => $street->street_id,
            'postal_code'     => '634057',
            'building_number' => 46,
            'literal'         => 1,
            'office'          => 36
        ]);

        $street = DB::table('street')
                    ->where('name', 'пр. Кирова')
                    ->first();

        DB::table('address')->insert([
            'country_id'      => $country->country_id,
            'region_id'       => $region->region_id,
            'city_id'         => $city->city_id,
            'street_id'       => $street->street_id,
            'postal_code'     => '634041',
            'building_number' => 48,
            'office'          => 24
        ]);

        $street = DB::table('street')
                    ->where('name', 'Дальнеключевская')
                    ->first();

        DB::table('address')->insert([
            'country_id'      => $country->country_id,
            'region_id'       => $region->region_id,
            'city_id'         => $city->city_id,
            'street_id'       => $street->street_id,
            'postal_code'     => '634026',
            'building_number' => 14,
            'literal'         => 'A',
            'office'          => 19
        ]);
    }
}
