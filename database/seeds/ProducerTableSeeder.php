<?php

use Illuminate\Database\Seeder;

class ProducerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = DB::table('city')
                  ->where('name', 'Северск')
                  ->first();

        $street = DB::table('street')
                    ->where('name', 'Предзаводская')
                    ->first();

        $address = DB::table('address')
                     ->where([
                         'city_id'         => $city->city_id,
                         'street_id'       => $street->street_id,
                         'building_number' => 14
                     ])
                     ->first();

        DB::table('producer')->insert([
            'producer_id'   => 956,
            'name'          => 'Деревенское молочко',
            'email'         => 'BagreevAM@dmilk.ru',
            'first_name'    => 'Артём',
            'last_name'     => 'Багреев',
            'middle_name'   => 'Михайлович',
            'contact_phone' => '+73823533343',
            'address_id'    => $address->address_id
        ]);

        $city = DB::table('city')
                  ->where('name', 'Томск')
                  ->first();

        $street = DB::table('street')
                    ->where('name', 'пр. Мира')
                    ->first();

        $address = DB::table('address')
                     ->where([
                         'city_id'         => $city->city_id,
                         'street_id'       => $street->street_id,
                         'building_number' => 20
                     ])
                     ->first();

        DB::table('producer')->insert([
            'producer_id'   => 826547,
            'name'          => 'KDV Group',
            'email'         => 'tomsk@kdv.ru',
            'first_name'    => 'Алексей',
            'last_name'     => 'Скворцов',
            'middle_name'   => 'Иванович',
            'contact_phone' => '+73822560056',
            'address_id'    => $address->address_id
        ]);
    }
}
