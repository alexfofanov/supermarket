#!/bin/bash

##############################################################
# VARIABLES declarations
##############################################################

APP_ENV_FILE="${APP_HOME}/.env.${APP_ENV}"

##############################################################
# End of VARIABLES declarations
##############################################################

##############################################################
# FUNCTIONS declarations
##############################################################

abort()
{
    echo $1 >&2
    exit $2
}

function Wait_For_Postgres() {
    echo "Checking if postgres is up..."

    DB_HOST=${DB_HOST:-postgres}
    DB_PORT=${DB_PORT:-5432}
    DB_USERNAME=${DB_USERNAME:-steamatic}
    DB_PASSWORD=${DB_PASSWORD:-steamatic}
    DB_DATABASE=${DB_DATABASE:-steamatic_nis}

    export PGHOST="${DB_HOST}"
    export PGPORT="${DB_PORT}"
    export PGUSER="${DB_USERNAME}"
    export PGPASSWORD="${DB_PASSWORD}"
    export PGDATABASE="${DB_DATABASE}"

    until psql -c '\l' > /dev/null 2>&1; do
        echo "Postgres' down. Waiting..."
        sleep 1
    done

    unset PGHOST PGPORT PGUSER PGPASSWORD PGDATABASE

    echo "Postgres' up."
}

function InitDb() {
    export PGHOST="${DB_HOST}"
    export PGPORT="${DB_PORT}"
    export PGUSER="${DB_USERNAME}"
    export PGPASSWORD="${DB_PASSWORD}"
    export PGDATABASE="${DB_DATABASE}"

    psql -a -f ${APP_HOME}/database/sql/supermarket.sql
}

function Echo_Message()
{
    printf '%*s\n' "80" '' | tr ' ' -
    echo "> $1"
    printf '%*s\n' "80" '' | tr ' ' -
}

function Install_Dependencies()
{
    composer install \
        --dev --prefer-dist --optimize-autoloader \
        --no-progress --no-suggest --no-interaction
}

function Run_Migrations()
{
    php artisan migrate --force
}

function Run_Tests()
{
    exec ./vendor/bin/phpunit
}

function Serve_In_Production()
{
    exec supervisord -c ./supervisor.production.server.conf
}

function Serve_With_Debug()
{
    XDEBUG_HOST=${XDEBUG_HOST:-172.17.0.1}
    XDEBUG_PORT=${XDEBUG_PORT:-9001}

    export PHP_IDE_CONFIG="serverName=Steamatic"
    export XDEBUG_CONFIG="${XDEBUG_CONFIG_BASE} remote_host=${XDEBUG_HOST} remote_port=${XDEBUG_PORT}"

    exec supervisord -c ./supervisor.production.server.conf
}

function Run_Dev_Server()
{
    XDEBUG_HOST=${XDEBUG_HOST:-172.17.0.1}
    XDEBUG_PORT=${XDEBUG_PORT:-9001}

    export PHP_IDE_CONFIG="serverName=Steamatic"
    export XDEBUG_CONFIG="${XDEBUG_CONFIG_BASE} remote_host=${XDEBUG_HOST} remote_port=${XDEBUG_PORT}"

    exec php artisan serve --port=80 --host=0.0.0.0
}

function Run_Startup_Tasks_For_Production()
{
    Echo_Message 'Checking database connection'
    if [[ ! -z ${DB_CONNECTION} ]] && [[ "${DB_CONNECTION}" != 'null' ]]; then
        Wait_For_Postgres
    fi

    Echo_Message 'Running cron in background'
    Run_Cron
}

function Run_Startup_Tasks_For_Development()
{
    Echo_Message 'Installing dependencies'
    Install_Dependencies

    Echo_Message 'Checking database connection'
    Wait_For_Postgres

    Echo_Message 'Running database schema migrations'
    Run_Migrations
}

function Run_Cron()
{
    mkdir -p /var/log/cron
    crond -b -L /var/log/cron/cron.log
}

##############################################################
# End of FUNCTIONS declarations
##############################################################
