<?php

namespace App\CRUD;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

class CustomizedCrudPanel extends CrudPanel
{
    use Create;
}
