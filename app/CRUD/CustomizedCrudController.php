<?php


namespace App\CRUD;


use Backpack\CRUD\app\Http\Controllers\CrudController;

class CustomizedCrudController extends CrudController
{
    public function __construct()
    {
        // call the setup function inside this closure to also have the request there
        // this way, developers can use things stored in session (auth variables, etc)
        $this->middleware(function ($request, $next) {
            $this->crud = new CustomizedCrudPanel();
            $this->request = $request;
            $this->setupDefaults();
            $this->setup();
            $this->setupConfigurationForCurrentOperation();

            return $next($request);
        });
    }
}
