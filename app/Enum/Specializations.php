<?php

namespace App\Enum;

use vijinho\Enums\Enum;

class Specializations extends Enum
{
    public const WAREHOUSEMAN = 'warehouseman';
    public const BUYER = 'buyer';
    public const CASHIER = 'cashier';

    protected static $values = [
        'WAREHOUSE' => self::WAREHOUSEMAN,
        'BUYER'     => self::BUYER,
        'CASHIER'   => self::CASHIER,
    ];
}
