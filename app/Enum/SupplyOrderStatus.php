<?php

namespace App\Enum;

use vijinho\Enums\Enum;

class SupplyOrderStatus extends Enum
{
    public const CREATED = 'created';
    public const ACCEPTED = 'accepted';
    public const DECLINED = 'declined';

    protected static $values = [
        'CREATED'  => self::CREATED,
        'ACCEPTED' => self::ACCEPTED,
        'DECLINED' => self::DECLINED,
    ];
}
