<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Street extends Model
{
    use CrudTrait;

    protected $table = 'street';

    protected $primaryKey = 'street_id';

    public $timestamps = false;

    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class, 'street_id', 'street_id');
    }

    protected $fillable = [
        'name',
    ];

    protected $rules = [
        'name' => 'required|unique:street',
    ];
}
