<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Region extends Model
{
    use CrudTrait;

    protected $table = 'region';

    protected $primaryKey = 'region_id';

    public $timestamps = false;

    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class, 'region_id', 'region_id');
    }

    protected $fillable = [
        'name',
    ];

    protected $rules = [
        'name' => 'required|unique:region',
    ];
}
