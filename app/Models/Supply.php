<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Supply extends Model
{
    use CrudTrait;

    const CREATED_AT = 'date';
    const UPDATED_AT = null;

    protected $table = 'supply';

    protected $primaryKey = 'supply_order_id';

    public $timestamps = true;

    public function supplyOrder(): BelongsTo
    {
        return $this->belongsTo(SupplyOrder::class, 'supply_order_id', 'supply_order_id');
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'employee_id');
    }

    public function products(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Product::class,
                'supplyorderproduct',
                'supply_order_id',
                'product_id'
            )
            ->withPivot([
                'price_per_item',
                'quantity'
            ]);
    }

    protected $fillable = [
        'supply_order_id',
        'employee_id',
        'date'
    ];

    protected $dates = [
        'date',
    ];

    protected $casts = [
        'date' => 'datetime:Y-m-d H:i:s\Z',
    ];

    protected $appends = [
        'total_price',
    ];

    public function getTotalPriceAttribute(): ?string
    {
        return $this->products->getTotalPrice() ?: 0;
    }
}
