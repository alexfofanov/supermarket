<?php

namespace App\Models;

use App\Enum\SupplyOrderStatus;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SupplyOrder extends Model
{
    use CrudTrait;

    const UPDATED_AT = null;

    protected $table = 'supplyorder';

    protected $primaryKey = 'supply_order_id';

    public $timestamps = true;

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'supplier_id');
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'employee_id');
    }

    public function supply(): HasOne
    {
        return $this->hasOne(Supply::class, 'supply_order_id', 'supply_order_id');
    }

    public function products(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Product::class,
                'supplyorderproduct',
                'supply_order_id',
                'product_id'
            )
            ->withPivot([
                'price_per_item',
                'quantity'
            ]);
    }

    protected $fillable = [
        'supplier_id',
        'employee_id',
        'accepted_at',
        'declined_at',
        'declined_reason',
    ];

    protected $dates = [
        'created_at',
        'accepted_at',
        'declined_at',
    ];

    protected $casts = [
        'created_at'  => 'datetime:Y-m-d H:i:s\Z',
        'accepted_at' => 'datetime:Y-m-d H:i:s\Z',
        'declined_at' => 'datetime:Y-m-d H:i:s\Z',
    ];

    protected $appends = [
        'total_price',
        'last_status',
        'last_status_change_at',
    ];

    public function getLastStatusAttribute(): string
    {
        $dates = [
            SupplyOrderStatus::CREATED  => $this->attributes['created_at'],
            SupplyOrderStatus::ACCEPTED => $this->attributes['accepted_at'],
            SupplyOrderStatus::DECLINED => $this->attributes['declined_at'],
        ];
        $max = max($dates);

        return array_search($max, $dates);
    }

    public function getLastStatusChangeAtAttribute(): string
    {
        $dates = [
            SupplyOrderStatus::CREATED  => $this->attributes['created_at'],
            SupplyOrderStatus::ACCEPTED => $this->attributes['accepted_at'],
            SupplyOrderStatus::DECLINED => $this->attributes['declined_at'],
        ];

        return max($dates);
    }

    public function getLastStatusHTML(): string
    {
        return ucfirst($this->getLastStatusAttribute());
    }

    public function getTotalPriceAttribute(): ?string
    {
        return $this->products->getTotalPrice() ?: 0;
    }
}
