<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use CrudTrait;

    protected $table = 'product';

    protected $primaryKey = 'product_id';

    public $timestamps = false;

    public function producer(): BelongsTo
    {
        return $this->belongsTo(Producer::class, 'producer_id', 'producer_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(ProductCategory::class, 'category_id', 'category_id');
    }

    public function products(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                SupplyOrder::class,
                'supplyorderproduct',
                'product_id',
                'supply_order_id'
            )
            ->withPivot([
                'price_per_item',
                'quantity'
            ]);
    }

    protected $fillable = [
        'producer_id',
        'product_number',
        'category_id',
        'name',
        'price',
    ];

    protected $casts = [
        'price' => 'float',
    ];

    protected $appends = [
        'full_name_with_totals',
        'full_name_with_quantity',
        'full_name',
    ];

    public function getFullNameAttribute(): string
    {
        $producerName = !empty($this->attributes['producer']) ?
            $this->attributes['producer']->name : $this->producer->name;

        $result = $this->attributes['name'];

        $result .= sprintf(' (%s)', $producerName);

        return $result;
    }

    public function getFullNameWithQuantityAttribute(): string
    {
        $pivotData = $this->relationsToArray()['pivot'];
        $result = sprintf(
            '%s x %s',
            $pivotData['quantity'],
            $this->getFullNameAttribute());

        return $result;
    }

    public function getFullNameWithTotalsAttribute(): string
    {
        $pivotData = $this->relationsToArray()['pivot'];
        $result = sprintf('%s: %.2f x %d = %.2f',
            $this->getFullNameAttribute(),
            $pivotData['price_per_item'],
            $pivotData['quantity'],
            round($pivotData['quantity'] * $pivotData['price_per_item'], 2),
        );

        return $result;
    }

    public function newCollection(array $models = [])
    {
        return new ProductsCollection($models);
    }
}
