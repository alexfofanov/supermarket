<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends Model
{
    use CrudTrait;

    protected $table = 'employee';

    protected $primaryKey = 'employee_id';

    public $timestamps = false;

    public function address(): BelongsTo
    {
        return $this->belongsTo(Address::class, 'address_id', 'address_id');
    }

    protected $fillable = [
        'address_id',
        'first_name',
        'last_name',
        'middle_name',
        'salary',
        'employment_date',
        'specialization',
    ];

    protected $dates = [
        'employment_date',
    ];

    protected $casts = [
        'employment_date' => 'datetime:Y-m-d',
        'salary'          => 'float',
    ];

    protected $appends = [
        'full_name'
    ];

    public function getFullNameAttribute()
    {
        $fullName = $this->attributes['last_name'] . ' ' . $this->attributes['first_name'];

        if (!empty($this->attributes['middle_name'])) {
            $fullName .= ' ' . $this->attributes['middle_name'];
        }

        return $fullName;
    }
}
