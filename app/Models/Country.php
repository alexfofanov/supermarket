<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Country extends Model
{
    use CrudTrait;

    protected $table = 'country';

    protected $primaryKey = 'country_id';

    public $timestamps = false;

    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class, 'country_id', 'country_id');
    }

    protected $fillable = [
        'name',
    ];

    protected $rules = [
        'name' => 'required|unique:country',
    ];
}
