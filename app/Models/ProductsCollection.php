<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

class ProductsCollection extends Collection
{
    public function getTotalPrice()
    {
        return $this->reduce(function ($total, $product) {
            $current = $product->pivot ? round(
                $product->pivot->price_per_item * $product->pivot->quantity,
            ) : 0;

            return $total + $current;
        });
    }
}
