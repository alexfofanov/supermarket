<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Supplier extends Model
{
    use CrudTrait;

    protected $table = 'supplier';

    protected $primaryKey = 'supplier_id';

    public $timestamps = false;

    public function address(): BelongsTo
    {
        return $this->belongsTo(Address::class, 'address_id', 'address_id');
    }

    protected $fillable = [
        'address_id',
        'first_name',
        'last_name',
        'middle_name',
        'email',
        'contact_phone',
        'contact_name',
    ];

    protected $appends = [
        'contact_name'
    ];

    public function getContactNameAttribute()
    {
        $fullName = $this->attributes['last_name'] . ' ' . $this->attributes['first_name'];

        if (!empty($this->attributes['middle_name'])) {
            $fullName .= ' ' . $this->attributes['middle_name'];
        }

        return $fullName;
    }
}
