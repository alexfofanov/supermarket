<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Address extends Model
{
    use CrudTrait;

    protected $table = 'address';

    protected $primaryKey = 'address_id';

    public $timestamps = false;

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id', 'country_id');
    }

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'region_id', 'region_id');
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id', 'city_id');
    }

    public function street(): BelongsTo
    {
        return $this->belongsTo(Street::class, 'street_id', 'street_id');
    }

    public function producers(): HasMany
    {
        return $this->hasMany(Producer::class, 'address_id', 'address_id');
    }

    protected $fillable = [
        'postal_code',
        'country_id',
        'region_id',
        'city_id',
        'street_id',
        'building_number',
        'literal',
        'office',
    ];

    protected $appends = [
        'full_address'
    ];

    public function getFullAddressAttribute()
    {
        $addressLine = $this->attributes['building_number'];
        if (!empty($this->attributes['literal'])) {
            $addressLine .= '/' . $this->attributes['literal'];
        }
        if (!empty($this->attributes['office'])) {
            $addressLine .= ', apt./office ' . $this->attributes['office'];
        }

        return implode(", ", [
            $this->attributes['postal_code'],
            !empty($this->attributes['country']) ? $this->attributes['country']->name : $this->country->name,
            !empty($this->attributes['region']) ? $this->attributes['region']->name : $this->region->name,
            !empty($this->attributes['city']) ? $this->attributes['city']->name : $this->city->name,
            !empty($this->attributes['street']) ? $this->attributes['street']->name : $this->street->name,
            $addressLine
        ]);
    }
}
