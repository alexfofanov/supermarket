<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductCategory extends Model
{
    use CrudTrait;

    protected $table = 'productcategory';

    protected $primaryKey = 'category_id';

    public $timestamps = false;

    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'category_id', 'category_id');
    }

    protected $fillable = [
        'name',
    ];
}
