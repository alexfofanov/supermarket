<?php

namespace App\Models;


use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model
{
    use CrudTrait;

    protected $table = 'city';

    protected $primaryKey = 'city_id';

    public $timestamps = false;

    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class, 'city_id', 'city_id');
    }

    protected $fillable = [
        'name',
    ];

    protected $rules = [
        'name' => 'required|unique:city',
    ];
}
