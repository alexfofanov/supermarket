<?php

namespace App\Http\Requests;

use App\Enum\Specializations;
use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_id'      => 'exists:address,address_id',
            'specialization'  => [
                'required',
                Rule::in(Specializations::values()),
            ],
            'first_name'      => 'required|min:3',
            'last_name'       => 'required|min:3',
            'middle_name'     => 'nullable|min:3',
            'salary'          => 'required|numeric',
            'employment_date' => 'required|date',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
