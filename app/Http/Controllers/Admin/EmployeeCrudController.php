<?php

namespace App\Http\Controllers\Admin;

use App\Enum\Specializations;
use App\Http\Requests\EmployeeRequest;
use App\Models\Address;
use App\Models\Employee;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class EmployeeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EmployeeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(Employee::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/employee');
        $this->crud->setEntityNameStrings(trans('backpack.employee'), trans('backpack.employees'));
        $this->crud->with(['address']);
    }

    protected function setupListOperation()
    {
        $this->crud->removeAllColumns();
        $this->crud->addColumns([
            'full_name',
            'specialization',
            'salary',
            'employment_date',
        ]);
        $this->crud->setColumnDetails('salary', [
            'type'   => 'number',
            'label'  => trans('backpack.salary'),
            'suffix' => ' руб.',
        ]);
        $this->crud->setColumnDetails('employment_date', [
            'type'   => 'date',
            'label'  => trans('backpack.employment_date'),
            'format' => 'DD.MM.YYYY',
        ]);
        $this->crud->setColumnDetails('full_name', [
            'label' => trans('backpack.full_name'),
        ]);
        $this->crud->setColumnDetails('specialization', [
            'label' => trans('backpack.specialization'),
        ]);
        $this->crud->addFilter([
            'name'  => 'specialization',
            'type'  => 'dropdown',
            'label' => trans('backpack.specialization'),
        ], [
            Specializations::BUYER        => trans('backpack.specialization_buyer'),
            Specializations::CASHIER      => trans('backpack.specialization_cashier'),
            Specializations::WAREHOUSEMAN => trans('backpack.specialization_warehouseman'),
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'specialization', $value);
        });
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(EmployeeRequest::class);

        $this->crud->addField([
            'label' => trans('backpack.last_name'),
            'type'  => 'text',
            'name'  => 'last_name',
        ]);
        $this->crud->addField([
            'label' => trans('backpack.first_name'),
            'type'  => 'text',
            'name'  => 'first_name',
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.middle_name'),
            'type'        => 'text',
            'name'        => 'middle_name',
            'allows_null' => true,
        ]);
        $this->crud->addField([
            'label'   => trans('backpack.specialization'),
            'type'    => 'select2_from_array',
            'name'    => 'specialization',
            'options' => array_flip(Specializations::values())
        ]);
        $this->crud->addField([
            'label'  => trans('backpack.salary'),
            'type'   => 'number',
            'name'   => 'salary',
            'suffix' => " руб.",
        ]);
        $this->crud->addField([
            'label' => trans('backpack.employment_date'),
            'type'  => 'date_picker',
            'name'  => 'employment_date',
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.address'),
            'type'        => 'select',
            'name'        => 'address_id',
            'entity'      => 'address',
            'attribute'   => 'full_address',
            'tableColumn' => false,
            'model'       => Address::class,
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->setupListOperation();

        $this->crud->addColumn([
            'label'       => trans('backpack.address'),
            'type'        => 'select',
            'name'        => 'address_id',
            'entity'      => 'address',
            'attribute'   => 'full_address',
            'tableColumn' => false,
            'model'       => Address::class,
        ]);
    }
}
