<?php

namespace App\Http\Controllers\Admin;

use App\CRUD\CustomizedCrudController;
use App\Enum\Specializations;
use App\Http\Requests\SupplyOrderRequest;
use App\Models\Employee;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\SupplyOrder;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SupplyOrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SupplyOrderCrudController extends CustomizedCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \App\CRUD\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(SupplyOrder::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/supplyorder');
        $this->crud->setEntityNameStrings(trans('backpack.supply_order'), trans('backpack.supply_orders'));
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            'supply_order_id',
            'supplier_id',
            'employee_id',
            'last_status',
            'last_status_change_at',
            'total_price',
        ]);

        $this->crud->setColumnDetails('supply_order_id', [
            'label' => trans('backpack.identifier'),
        ]);
        $this->crud->setColumnDetails('supplier_id', [
            'label'       => trans('backpack.supplier'),
            'type'        => 'select',
            'name'        => 'supplier_id',
            'entity'      => 'supplier',
            'attribute'   => 'name',
            'model'       => Supplier::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('employee_id', [
            'label'       => trans('backpack.specialization_buyer'),
            'type'        => 'select',
            'name'        => 'employee_id',
            'entity'      => 'employee',
            'attribute'   => 'full_name',
            'model'       => Employee::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('last_status', [
            'name'          => 'last_status',
            'label'         => trans('backpack.last_status'),
            'type'          => 'model_function',
            'function_name' => 'getLastStatusHTML',
            'limit'         => 100
        ]);
        $this->crud->setColumnDetails('last_status_change_at', [
            'type'        => 'datetime',
            'searchLogic' => false,
            'label'       => trans('backpack.last_status_change'),
        ]);
        $this->crud->setColumnDetails('total_price', [
            'label'    => trans('backpack.supply_order_total'),
            'type'     => 'number',
            'name'     => 'total_price',
            'suffix'   => " руб.",
            'decimals' => 2
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SupplyOrderRequest::class);

        $this->crud->addField([
            'label'     => trans('backpack.supplier'),
            'type'      => 'select',
            'name'      => 'supplier_id',
            'entity'    => 'supplier',
            'attribute' => 'name',
            'model'     => Supplier::class,
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.specialization_buyer'),
            'type'      => 'select',
            'name'      => 'employee_id',
            'entity'    => 'employee',
            'attribute' => 'full_name',
            'model'     => Employee::class,
            'options'   => (function (Builder $query) {
                return $query->where('specialization', '=', Specializations::BUYER)
                             ->get();
            }),
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.products'),
            'type'        => 'select2_multiple_product',
            'name'        => 'products',
            'entity'      => 'products',
            'attribute'   => 'full_name',
            'pivot'       => true,
            'pivotFields' => [
                'price_per_item' => trans('backpack.price_per_item'),
                'quantity'       => trans('backpack.quantity')
            ],
            'model'       => Product::class,
            'multiple'    => true
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->addField([
            'label'     => trans('backpack.supplier'),
            'type'      => 'select',
            'name'      => 'supplier_id',
            'entity'    => 'supplier',
            'attribute' => 'name',
            'model'     => Supplier::class,
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.specialization_buyer'),
            'type'      => 'select',
            'name'      => 'employee_id',
            'entity'    => 'employee',
            'attribute' => 'full_name',
            'model'     => Employee::class,
            'options'   => (function (Builder $query) {
                return $query->where('specialization', '=', Specializations::BUYER)
                             ->get();
            }),
        ]);
        $this->crud->addField([
            'label'                   => trans('backpack.accepted_at'),
            'type'                    => 'datetime_picker',
            'name'                    => 'accepted_at',
            'datetime_picker_options' => [
                'format'   => 'YYYY-MM-DD HH:mm',
                'language' => 'ru'
            ],
            'allows_null'             => true,
        ]);
        $this->crud->addField([
            'label'                   => trans('backpack.declined_at'),
            'type'                    => 'datetime_picker',
            'name'                    => 'declined_at',
            'datetime_picker_options' => [
                'format'   => 'YYYY-MM-DD HH:mm',
                'language' => 'ru'
            ],
            'allows_null'             => true,
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.decline_reason'),
            'type'        => 'text',
            'name'        => 'declined_reason',
            'allows_null' => true,
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.products'),
            'type'        => 'select2_multiple_product',
            'name'        => 'products',
            'entity'      => 'products',
            'attribute'   => 'full_name',
            'pivot'       => true,
            'pivotFields' => [
                'price_per_item' => trans('backpack.price_per_item'),
                'quantity'       => trans('backpack.quantity')
            ],
            'model'       => Product::class,
            'multiple'    => true
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->addColumns([
            'supply_order_id',
            'supplier_id',
            'employee_id',
            'last_status',
            'last_status_change_at',
            'products',
            'total_price',
            'created_at',
            'accepted_at',
            'declined_at',
            'declined_reason'
        ]);

        $this->crud->setColumnDetails('supply_order_id', [
            'label' => trans('backpack.identifier'),
        ]);
        $this->crud->setColumnDetails('supplier_id', [
            'label'       => trans('backpack.supplier'),
            'type'        => 'select',
            'name'        => 'supplier_id',
            'entity'      => 'supplier',
            'attribute'   => 'name',
            'model'       => Supplier::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('employee_id', [
            'label'       => trans('backpack.specialization_buyer'),
            'type'        => 'select',
            'name'        => 'employee_id',
            'entity'      => 'employee',
            'attribute'   => 'full_name',
            'model'       => Employee::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('last_status', [
            'name'          => 'last_status',
            'label'         => trans('backpack.last_status'),
            'type'          => 'model_function',
            'function_name' => 'getLastStatusHTML',
            'limit'         => 100
        ]);
        $this->crud->setColumnDetails('last_status_change_at', [
            'type'        => 'datetime',
            'searchLogic' => false,
            'label'       => trans('backpack.last_status_change'),
        ]);

        $this->crud->setColumnDetails('products', [
            'label'     => trans('backpack.products'),
            'type'      => 'select_multiple',
            'name'      => 'products',
            'entity'    => 'products',
            'attribute' => 'full_name_with_quantity',
            'model'     => Product::class,
        ]);
        $this->crud->setColumnDetails('created_at', [
            'type'  => 'datetime',
            'label' => trans('backpack.created_at'),
        ]);
        $this->crud->setColumnDetails('accepted_at', [
            'type'  => 'datetime',
            'label' => trans('backpack.accepted_at'),
        ]);
        $this->crud->setColumnDetails('declined_at', [
            'type'  => 'datetime',
            'label' => trans('backpack.declined_at'),
        ]);
        $this->crud->setColumnDetails('declined_reason', [
            'label' => trans('backpack.decline_reason'),
        ]);
        $this->crud->setColumnDetails('total_price', [
            'label'      => trans('backpack.supply_order_total'),
            'type'       => 'number',
            'name'       => 'total_price',
            'suffix'     => " руб.",
            'attributes' => ["step" => "any"],
            'decimals'   => 2
        ]);
    }
}
