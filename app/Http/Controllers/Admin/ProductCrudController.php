<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Producer;
use App\Models\Product;
use App\Models\ProductCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(Product::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings(trans('backpack.product'), trans('backpack.products'));
        $this->crud->with(['producer', 'category']);
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            'producer_id',
            'product_number',
            'category_id',
            'name',
            'price'
        ]);
        $this->crud->setColumnDetails('name', [
            'label' => trans('backpack.product_name'),
        ]);
        $this->crud->setColumnDetails('producer_id', [
            'label'       => trans('backpack.producer'),
            'type'        => 'select',
            'name'        => 'producer_id',
            'entity'      => 'producer',
            'attribute'   => 'name',
            'model'       => Producer::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('product_number', [
            'type'          => 'number',
            'label'         => trans('backpack.product_number'),
            'decimals'      => 0,
            'dec_point'     => '',
            'thousands_sep' => '',
        ]);
        $this->crud->setColumnDetails('category_id', [
            'label'       => trans('backpack.product_category'),
            'type'        => 'select',
            'name'        => 'category_id',
            'entity'      => 'category',
            'attribute'   => 'name',
            'model'       => ProductCategory::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('price', [
            'label'       => trans('backpack.product_price'),
            'type'        => 'number',
            'decimals'    => 2,
            'suffix'      => " руб.",
            'searchLogic' => false,
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProductCreateRequest::class);

        $this->crud->addField([
            'name'  => 'name',
            'type'  => 'text',
            'label' => trans('backpack.product_name'),
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.producer'),
            'type'      => 'select',
            'name'      => 'producer_id',
            'entity'    => 'producer',
            'attribute' => 'name',
            'model'     => Producer::class,
        ]);
        $this->crud->addField([
            'label' => trans('backpack.product_number'),
            'type'  => 'number',
            'name'  => 'product_number',
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.product_category'),
            'type'      => 'select',
            'name'      => 'category_id',
            'entity'    => 'category',
            'attribute' => 'name',
            'model'     => ProductCategory::class,
        ]);
        $this->crud->addField([
            'label'      => trans('backpack.product_price'),
            'type'       => 'number',
            'name'       => 'price',
            'suffix'     => " руб.",
            'attributes' => ["step" => "any"],
            'decimals'   => 2
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setValidation(ProductUpdateRequest::class);

        $this->crud->addField([
            'name'  => 'name',
            'type'  => 'text',
            'label' => trans('backpack.product_name'),
        ]);
        $this->crud->addField([
            'label'      => trans('backpack.producer'),
            'type'       => 'select',
            'name'       => 'producer_id',
            'entity'     => 'producer',
            'attribute'  => 'name',
            'model'      => Producer::class,
            'attributes' => [
                'readonly' => true,
            ],
        ]);
        $this->crud->addField([
            'label'      => trans('backpack.product_number'),
            'type'       => 'number',
            'name'       => 'product_number',
            'attributes' => [
                'readonly' => true,
            ],
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.product_category'),
            'type'      => 'select',
            'name'      => 'category_id',
            'entity'    => 'category',
            'attribute' => 'name',
            'model'     => ProductCategory::class,
        ]);
        $this->crud->addField([
            'label'      => trans('backpack.product_price'),
            'type'       => 'number',
            'name'       => 'price',
            'suffix'     => " руб.",
            'attributes' => ["step" => "any"],
            'decimals'   => 2
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->setupListOperation();
    }
}
