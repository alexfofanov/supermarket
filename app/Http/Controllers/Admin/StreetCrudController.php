<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StreetRequest;
use App\Models\Street;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class StreetCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class StreetCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(Street::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/street');
        $this->crud->setEntityNameStrings(trans('backpack.street'), trans('backpack.streets'));
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'label' => trans('backpack.name'),
            'name'  => 'name',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(StreetRequest::class);

        $this->crud->addField([
            'name'  => 'name',
            'type'  => 'text',
            'label' => trans('backpack.name')
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->setupListOperation();
    }
}
