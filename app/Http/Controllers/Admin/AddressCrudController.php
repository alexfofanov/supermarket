<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddressRequest;
use App\Models\Address;
use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use App\Models\Street;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class AddressCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AddressCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(Address::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/address');
        $this->crud->setEntityNameStrings(trans('backpack.address'), trans('backpack.addresses'));
        $this->crud->with(['country', 'region', 'city', 'street']);
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'label' => trans('backpack.postal_code'),
            'type'  => 'text',
            'name'  => 'postal_code',
        ]);
        $this->crud->addColumn([
            'label'     => trans('backpack.country'),
            'type'      => 'select',
            'name'      => 'country_id',
            'entity'    => 'country',
            'attribute' => 'name',
            'model'     => Country::class,
        ]);
        $this->crud->addColumn([
            'label'     => trans('backpack.region'),
            'type'      => 'select',
            'name'      => 'region_id',
            'entity'    => 'region',
            'attribute' => 'name',
            'model'     => Region::class,
        ]);
        $this->crud->addColumn([
            'label'     => trans('backpack.city'),
            'type'      => 'select',
            'name'      => 'city_id',
            'entity'    => 'city',
            'attribute' => 'name',
            'model'     => City::class,
        ]);
        $this->crud->addColumn([
            'label'     => trans('backpack.street'),
            'type'      => 'select',
            'name'      => 'street_id',
            'entity'    => 'street',
            'attribute' => 'name',
            'model'     => Street::class,
        ]);
        $this->crud->addColumn([
            'label' => trans('backpack.building_number'),
            'type'  => 'text',
            'name'  => 'building_number',
        ]);
        $this->crud->addColumn([
            'label' => trans('backpack.literal'),
            'type'  => 'text',
            'name'  => 'literal',
        ]);
        $this->crud->addColumn([
            'label' => trans('backpack.apt_office'),
            'type'  => 'text',
            'name'  => 'office',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AddressRequest::class);

        $this->crud->addField([
            'label' => trans('backpack.postal_code'),
            'type'  => 'text',
            'name'  => 'postal_code',
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.country'),
            'type'      => 'select',
            'name'      => 'country_id',
            'entity'    => 'country',
            'attribute' => 'name',
            'model'     => Country::class,
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.region'),
            'type'      => 'select',
            'name'      => 'region_id',
            'entity'    => 'region',
            'attribute' => 'name',
            'model'     => Region::class,
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.city'),
            'type'      => 'select',
            'name'      => 'city_id',
            'entity'    => 'city',
            'attribute' => 'name',
            'model'     => City::class,
        ]);
        $this->crud->addField([
            'label'     => trans('backpack.street'),
            'type'      => 'select',
            'name'      => 'street_id',
            'entity'    => 'street',
            'attribute' => 'name',
            'model'     => Street::class,
        ]);
        $this->crud->addField([
            'label' => trans('backpack.building_number'),
            'type'  => 'text',
            'name'  => 'building_number',
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.literal'),
            'type'        => 'text',
            'name'        => 'literal',
            'allows_null' => true,
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.apt_office'),
            'type'        => 'text',
            'name'        => 'office',
            'allows_null' => true,
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->setupListOperation();
    }
}
