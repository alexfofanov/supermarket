<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProducerRequest;
use App\Models\Address;
use App\Models\Producer;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class ProducerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProducerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(Producer::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/producer');
        $this->crud->setEntityNameStrings(trans('backpack.producer'), trans('backpack.producers'));
        $this->crud->with(['address']);
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            'producer_id',
            'contact_name',
            'email',
            'contact_phone',
        ]);
        $this->crud->setColumnDetails('producer_id', [
            'type'          => 'number',
            'label'         => trans('backpack.identifier'),
            'decimals'      => 0,
            'dec_point'     => '',
            'thousands_sep' => '',
        ]);
        $this->crud->setColumnDetails('contact_name', [
            'label' => trans('backpack.contact_name'),
        ]);
        $this->crud->setColumnDetails('email', [
            'label' => trans('backpack.email'),
        ]);
        $this->crud->setColumnDetails('contact_phone', [
            'label' => trans('backpack.contact_phone'),
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProducerRequest::class);

        $this->crud->addField([
            'label' => trans('backpack.identifier'),
            'type'  => 'number',
            'name'  => 'producer_id',
        ]);
        $this->crud->addField([
            'label' => trans('backpack.contact_last_name'),
            'type'  => 'text',
            'name'  => 'last_name',
        ]);
        $this->crud->addField([
            'label' => trans('backpack.contact_first_name'),
            'type'  => 'text',
            'name'  => 'first_name',
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.contact_middle_name'),
            'type'        => 'text',
            'name'        => 'middle_name',
            'allows_null' => true,
        ]);
        $this->crud->addField([
            'label' => trans('backpack.contact_phone'),
            'type'  => 'text',
            'name'  => 'contact_phone',
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.address'),
            'type'        => 'select',
            'name'        => 'address_id',
            'entity'      => 'address',
            'attribute'   => 'full_address',
            'tableColumn' => false,
            'model'       => Address::class,
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->setupListOperation();

        $this->crud->addColumn([
            'label'       => trans('backpack.address'),
            'type'        => 'select',
            'name'        => 'address_id',
            'entity'      => 'address',
            'attribute'   => 'full_address',
            'tableColumn' => false,
            'model'       => Address::class,
        ]);
    }
}
