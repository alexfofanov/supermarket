<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RegionRequest;
use App\Models\Region;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;

/**
 * Class RegionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class RegionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(Region::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/region');
        $this->crud->setEntityNameStrings(trans('backpack.region'), trans('backpack.regions'));
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'label' => trans('backpack.name'),
            'name'  => 'name',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(RegionRequest::class);

        $this->crud->addField([
            'name'  => 'name',
            'type'  => 'text',
            'label' => trans('backpack.name')
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->setupListOperation();
    }
}
