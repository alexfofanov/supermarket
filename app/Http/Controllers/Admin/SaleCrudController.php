<?php

namespace App\Http\Controllers\Admin;

use App\CRUD\CustomizedCrudController;
use App\Enum\Specializations;
use App\Http\Requests\SaleRequest;
use App\Models\Employee;
use App\Models\Product;
use App\Models\Sale;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SaleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SaleCrudController extends CustomizedCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \App\CRUD\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel(Sale::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/sale');
        $this->crud->setEntityNameStrings(trans('backpack.sale'), trans('backpack.sales'));
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            'sale_id',
            'employee_id',
            'date',
            'total_price',
        ]);
        $this->crud->setColumnDetails('sale_id', [
            'label' => trans('backpack.identifier'),
        ]);
        $this->crud->setColumnDetails('employee_id', [
            'label'       => trans('backpack.specialization_cashier'),
            'type'        => 'select',
            'name'        => 'employee_id',
            'entity'      => 'employee',
            'attribute'   => 'full_name',
            'model'       => Employee::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('date', [
            'type'        => 'datetime',
            'searchLogic' => false,
            'label'       => trans('backpack.date_and_time'),
        ]);
        $this->crud->setColumnDetails('total_price', [
            'label'    => trans('backpack.sale_total'),
            'type'     => 'number',
            'name'     => 'total_price',
            'suffix'   => " руб.",
            'decimals' => 2
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SaleRequest::class);

        $this->crud->addField([
            'label'     => trans('backpack.specialization_cashier'),
            'type'      => 'select',
            'name'      => 'employee_id',
            'entity'    => 'employee',
            'attribute' => 'full_name',
            'model'     => Employee::class,
            'options'   => (function (Builder $query) {
                return $query->where('specialization', '=', Specializations::CASHIER)
                             ->get();
            }),
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.products'),
            'type'        => 'select2_multiple_product_price_readonly',
            'name'        => 'products',
            'entity'      => 'products',
            'attribute'   => 'full_name',
            'pivot'       => true,
            'pivotFields' => [
                'price_per_item' => trans('backpack.price_per_item'),
                'quantity'       => trans('backpack.quantity')
            ],
            'model'       => Product::class,
            'multiple'    => true
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setValidation(SaleRequest::class);

        $this->crud->addField([
            'label'      => trans('backpack.specialization_cashier'),
            'type'       => 'select',
            'name'       => 'employee_id',
            'entity'     => 'employee',
            'attribute'  => 'full_name',
            'model'      => Employee::class,
            'options'    => (function (Builder $query) {
                return $query->where('specialization', '=', Specializations::CASHIER)
                             ->get();
            }),
            'attributes' => [
                'readonly' => true,
            ],
        ]);
        $this->crud->addField([
            'label'       => trans('backpack.products'),
            'type'        => 'select2_multiple_product_price_readonly',
            'name'        => 'products',
            'entity'      => 'products',
            'attribute'   => 'full_name',
            'pivot'       => true,
            'pivotFields' => [
                'price_per_item' => trans('backpack.price_per_item'),
                'quantity'       => trans('backpack.quantity')
            ],
            'model'       => Product::class,
            'multiple'    => true
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->addColumns([
            'sale_id',
            'employee_id',
            'date',
            'total_price',
            'products'
        ]);
        $this->crud->setColumnDetails('sale_id', [
            'label' => trans('backpack.identifier'),
        ]);
        $this->crud->setColumnDetails('employee_id', [
            'label'       => trans('backpack.specialization_cashier'),
            'type'        => 'select',
            'name'        => 'employee_id',
            'entity'      => 'employee',
            'attribute'   => 'full_name',
            'model'       => Employee::class,
            'searchLogic' => false,
        ]);
        $this->crud->setColumnDetails('date', [
            'type'        => 'datetime',
            'searchLogic' => false,
            'label'       => trans('backpack.date_and_time'),
        ]);
        $this->crud->setColumnDetails('total_price', [
            'label'    => trans('backpack.sale_total'),
            'type'     => 'number',
            'name'     => 'total_price',
            'suffix'   => " руб.",
            'decimals' => 2
        ]);
        $this->crud->setColumnDetails('products', [
            'label'     => trans('backpack.products'),
            'type'      => 'select_multiple',
            'name'      => 'products',
            'entity'    => 'products',
            'attribute' => 'full_name_with_totals',
            'model'     => Product::class,
        ]);
    }
}
