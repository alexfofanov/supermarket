<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('country', 'CountryCrudController');
    Route::crud('region', 'RegionCrudController');
    Route::crud('city', 'CityCrudController');
    Route::crud('street', 'StreetCrudController');
    Route::crud('address', 'AddressCrudController');
    Route::crud('producer', 'ProducerCrudController');
    Route::crud('supplier', 'SupplierCrudController');
    Route::crud('employee', 'EmployeeCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('productcategory', 'ProductCategoryCrudController');
    Route::crud('supplyorder', 'SupplyOrderCrudController');
    Route::crud('sale', 'SaleCrudController');
    Route::crud('supply', 'SupplyCrudController');
}); // this should be the absolute last line of this file