<?php

return [

    'countries' => 'Страны',
    'country'   => 'Страна',

    'regions' => 'Регионы',
    'region'  => 'Регион',

    'cities' => 'Города',
    'city'   => 'Город',

    'streets' => 'Улицы',
    'street'  => 'Улица',

    'addresses' => 'Адреса',
    'address'   => 'Адрес',

    'name'            => 'Название',
    'postal_code'     => 'Индекс',
    'literal'         => 'Литера',
    'building_number' => 'Номер дома',
    'apt_office'      => 'Номер офиса/квартиры',

    'employees' => 'Сотрудники',
    'employee'  => 'Сотрудник',

    'producers' => 'Производители',
    'producer'  => 'Производитель',

    'suppliers' => 'Поставщики',
    'supplier'  => 'Поставщик',

    'identifier'                  => 'Идентификатор',
    'full_name'                   => 'ФИО',
    'first_name'                  => 'Фамилия',
    'last_name'                   => 'Имя',
    'middle_name'                 => 'Отчество',
    'salary'                      => 'Зарплата',
    'email'                       => 'Электронная почта',
    'specialization'              => 'Специализация',
    'specialization_buyer'        => 'Закупщик',
    'specialization_cashier'      => 'Кассир',
    'specialization_warehouseman' => 'Кладовщик',
    'employment_date'             => 'Дата трудоустройства',
    'contact_name'                => 'Контактное лицо',
    'contact_phone'               => 'Контактный телефон',
    'contact_first_name'          => 'Фамилия (контактного лица)',
    'contact_last_name'           => 'Имя (контактного лица)',
    'contact_middle_name'         => 'Отчество (контактного лица)',

    'products' => 'Товары',
    'product'  => 'Товар',

    'product_categories' => 'Категории товаров',
    'product_category'   => 'Категория товара',

    'product_name'   => 'Наименование',
    'product_number' => 'Номер',
    'product_price'  => 'Цена',

    'sales' => 'Продажи',
    'sale'  => 'Продажа',

    'supply_orders' => 'Заявки на поставку',
    'supply_order'  => 'Заявка на поставку',

    'supplies' => 'Поставки',
    'supply'   => 'Поставка',

    'sale_total'         => 'Общая стоимость',
    'supply_total'       => 'Общая стоимость',
    'supply_order_total' => 'Общая стоимость',
    'price_per_item'     => 'Цена (за шт.)',
    'quantity'           => 'Количество',
    'date'               => 'Дата',
    'date_and_time'      => 'Дата и время',
    'last_status'        => 'Последний статус',
    'last_status_change' => 'Время последнего изменения статуса',
    'created_at'         => 'Создана',
    'accepted_at'        => 'Принята',
    'declined_at'        => 'Отменена',
    'decline_reason'     => 'Причина отмены',
];

