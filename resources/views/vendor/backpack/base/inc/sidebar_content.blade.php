<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="fa fa-home nav-icon"></i> {{ trans('backpack.addresses') }}
    </a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('country') }}'><i class='nav-icon fa fa-folder '></i> {{ trans('backpack.countries') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('region') }}'><i class='nav-icon fa fa-folder '></i> {{ trans('backpack.regions') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('city') }}'><i class='nav-icon fa fa-folder '></i> {{ trans('backpack.cities') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('street') }}'><i class='nav-icon fa fa-folder '></i> {{ trans('backpack.streets') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('address') }}'><i class='nav-icon fa fa-folder'></i> {{ trans('backpack.addresses') }}</a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('employee') }}'><i class='nav-icon fa fa-users'></i> {{  trans('backpack.employees') }}</a></li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="fa fa-shopping-cart nav-icon"></i> {{ trans('backpack.products') }}
    </a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('productcategory') }}'><i class='nav-icon fa fa-tags'></i> {{ trans('backpack.product_categories') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon fa fa-cube'></i> {{ trans('backpack.products') }}</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="fa fa-cubes nav-icon"></i> {{ trans('backpack.supplies') }}
    </a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('producer') }}'><i class='nav-icon fa fa-gears'></i> {{ trans('backpack.producers') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('supplier') }}'><i class='nav-icon fa fa-truck'></i> {{ trans('backpack.suppliers') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('supplyorder') }}'><i class='nav-icon fa fa-bell'></i> {{ trans('backpack.supply_orders') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('supply') }}'><i class='nav-icon fa fa-arrow-circle-down'></i> {{ trans('backpack.supplies') }}</a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('sale') }}'><i class='nav-icon fa fa-money'></i> {{ trans('backpack.sales') }}</a></li>
