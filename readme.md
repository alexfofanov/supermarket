# Курсовой проект по дисциплине "Базы данных"

Предметная область - Супермаркет

## Онлайн-демо

Онлайн демо доступно по ссылке: https://supermarket.aleksfofanov.me
Пожалуйста, используйте следующие логин и пароль для входа:
`demo@example.com` / `123456`

## Развертывание локально

### Требования
 
Для развертывания локально необходимы:
- ОС на базе ядра Linux
- [Docker](https://docs.docker.com/install/) (~18.06)
- [docker-compose](https://docs.docker.com/compose/install/) (~1.22)

### Инструкции

1. Скачать содержимое данного репозитория и в командной строке перейти
в папку, куда содержимое было скачано.

2. Создать файлы настроек из примеров:
    ```bash
    cp .env.compose.example .env
    cp .env.local.example .env.local
    ```

3. Запустить СУБД и приложение:
    ```bash
    docker-compose up -d
    ```

После этого приложение будет доступно по адресу
[localhost:8000](http://localhost:8000).

Для того, чтобы заполнить таблицы тестовыми данными, выполните следующую
команду:
```bash
docker-compose exec backend php artisan db:seed
```

При первом запуске, приложение автоматически создаст базу данных
с помощью sql скрипта (хранится в папке: `database/sql/supermarket.sql`,
созданного из физической модели БД, которая, в свою очередь, создана из
концептуальной модели предметной области.
