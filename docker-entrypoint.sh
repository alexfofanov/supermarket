#!/usr/bin/env bash

set -e

. ./docker/scripts/entrypoint-helpers.sh

if [[ "${1}" = "run_migrations" ]]
then
    Run_Migrations
elif [[ "${1}" = "init_db" ]]
then
    InitDb
elif [[ "${1}" = "serve" ]]
then
    Echo_Message 'Running startup tasks'
    Run_Startup_Tasks_For_Production

    Echo_Message 'Launching PHP-FPM in production mode'
    Serve_In_Production
elif [[ "${1}" = "dev_server" ]]
then
    export APP_ENV=local
    export APP_DEBUG=true

    Echo_Message 'Running startup tasks'
    Run_Startup_Tasks_For_Development

    Echo_Message 'Launching dev server'
    Run_Dev_Server
else
    exec "$@"
fi
